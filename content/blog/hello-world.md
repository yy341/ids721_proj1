+++
title = "Hello All in IDS721!"
date = 2024-01-29T21:00:00+00:00
updated = 2024-01-29T21:00:00+00:00
draft = false
template = "blog/page.html"

[taxonomies]
authors = ["Yucheng Yang"]

[extra]
lead = "This is the kindest greetings to all in <b>IDS721</b> in 2024 Spring from <b>Yucheng Yang</b>."
+++

```rust
// This is a comment, and is ignored by the compiler

// This is the main function
fn main() {
    // Statements here are executed when the compiled binary is called

    // Print text to the console
    println!("Hello All in IDS721!");
}
```

`println!` is a macro that prints text to the console.

A binary can be generated using the Rust compiler: `rustc`.

```bash
$ rustc hello.rs
```

`rustc` will produce a `hello` binary that can be executed.

```bash
$ ./hello
Hello World!
```
