+++
title = "IDS721 Personal Portofolio"


# The homepage contents
[extra]
lead = '<b>Yucheng Yang</b> is the owner and the developer of this portofolio website and work'
url = "docs/projects/project1/"
url_button = "Get started"
repo_version = "GitLab"
repo_license = "Revised by Yucheng Yang. View repo at:"
repo_url = "https://gitlab.com/DavidYang829/miniproj_1"

# Menu items
[[extra.menu.main]]
name = "Docs"
section = "docs"
url = "docs/projects/project1/"
weight = 10

[[extra.menu.main]]
name = "Blog"
section = "blog"
url = "/blog/"
weight = 20

[[extra.list]]
title = "Security aware"
content = 'Get A+ scores on <a href="https://observatory.mozilla.org/analyze/adidoks.org">Mozilla Observatory</a> out of the box. Easily change the default Security Headers to suit your needs.'

[[extra.list]]
title = "Fast by default ⚡️"
content = 'Get 100 scores on <a href="https://googlechrome.github.io/lighthouse/viewer/?gist=7731347bb8ce999eff7428a8e763b637">Google Lighthouse</a> by default. Doks removes unused css, prefetches links, and lazy loads images.'

[[extra.list]]
title = "SEO-ready"
content = "Use sensible defaults for structured data, open graph, and Twitter cards. Or easily change the SEO settings to your liking."

[[extra.list]]
title = "Full text search"
content = "Search your Doks site with FlexSearch. Easily customize index settings and search options to your liking."

[[extra.list]]
title = "Page layouts"
content = "Build pages with a landing page, blog, or documentation layout. Add custom sections and components to suit your needs."

[[extra.list]]
title = "Dark mode"
content = "Switch to a low-light UI with the click of a button. Change colors with variables to match your branding."

+++
